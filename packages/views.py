from queue import Empty
from tracemalloc import start
from unittest import result
from django.shortcuts import render
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import mixins
from rest_framework import generics, authentication, permissions
import os
import requests
from dotenv import load_dotenv

load_dotenv()

KTAPI_BASE_URL = os.getenv('KTAPI_BASE_URL')
API_KEY = os.getenv('API_KEY')
API_SECRET = os.getenv('API_SECRET')
HEADER = os.getenv('HEADER')
TAX_PROFILE_ID = os.getenv('TAX_PROFILE_ID')
CHANNEL_ID = os.getenv('CHANNEL_ID')
# Create your views here.

#Gets all users in database
class PackagesListView(generics.ListAPIView):
    queryset = ""
    serializer_class = ""
    permission_classes = [AllowAny,]

    def get(self, request):
        url = self.create_url(request=request)
        req = requests.get(url, headers={"Authorization": HEADER})
        req_json = req.json()
        try:
            if req_json['code']:
                return Response(req_json, status=400)
        except:
            res = filter_response(results = req.json()['results'])
            if res:
                return Response(res, status=200)
            else:
                return Response([], status=req.status_code) 
        
    

    def create_url(self, request):
        url = f'{KTAPI_BASE_URL}/v1.0/packages/search?tax_profile_id={TAX_PROFILE_ID}&channel_id={CHANNEL_ID}&currency=EUR'
        try:
            occupancy = request.query_params.get('occupancy')
            if occupancy:
                url = f'{url}&occupancy={occupancy}'
        except:
            pass
        try:
            date_from = request.query_params.get('date_from')
            if date_from:
                url = f'{url}&date_from={date_from}'
        except:
            pass
        try:
            date_to = request.query_params.get('date_to')
            if date_to:
                url = f'{url}&date_to={date_to}'
        except:
            pass
        try:
            limit = request.query_params.get('limit')
            if limit:
                url = f'{url}&limit={limit}'
        except:
            pass
        try:
            filter= request.query_params.get('filter')
            if filter:
                url = f'{url}&filter={filter}'
        except:
            pass
        return url


class SinglePackageView(generics.ListAPIView):
    queryset = ""
    serializer_class = ""
    permission_classes = [AllowAny,]

    def get(self, request, pk):
        packages_url = f'{KTAPI_BASE_URL}/v1.0/packages/search?tax_profile_id={TAX_PROFILE_ID}&channel_id={CHANNEL_ID}&currency=EUR&occupancy=1=1,0&limit=1&filter=id=={pk}'
        departures_url = f'{KTAPI_BASE_URL}/v1.0/packages/{pk}/departures'
        req = requests.get(packages_url, headers={"Authorization": HEADER})
        departures = requests.get(departures_url, headers={"Authorization": HEADER})
        try:
            if(req.json()['results'] != []):
                response = filter_single_response(result=req.json()['results'][0], departures=departures.json())
                
                return Response(response, status=200)
            else:
                message = {'Error': 'No packages found that match the given parameters'}
                return Response(message, status=400)
        except:
            return Response(req.json(), status=req.status_code)
    

def filter_response(results):
    packages = []
    for item in results:
        package = item['package']
        prices = item['prices_by_service_level']
        response = {}
        try:
            images = package['images']
            if images[0]:
                response['image'] = images[0]['url']
        except:
            images_error = True
            print('Error fetching image')
        try:
            id = package['id']
            if id:
                response['id'] = id
        except:
            id_error = True
            print('Error fetching id')
        try:
            length = package['length']
            if length:
                response['length'] = length
        except:
            length_error = True
            print('Error fetching length')
        try:
            category = package['category']
            if category:
                response['category'] = category
        except:
            category_error = True
            print('Error fetching category')
        try:
            name = package['name']
            if name:
                response['name'] = name
        except:
            name_error = True
            print('Error fetching name')
        try:
            description = package['description']
            if description:
                response['description'] = description
        except:
            description_error = True
            print('Error fetching description')
        try:
            start_location = package['start_location']
            if start_location:
                response['start_location'] = start_location
            print(start_location)
        except:
            start_location_error = True
            print('Error fetching start location')
        try:
            end_location = package['end_location']
            if end_location:
                response['end_location'] = end_location
        except:
            end_location_error = True
            print('Error fetching end location')
        try:
            total_price = prices[0]['total_price']
            if total_price:
                response['total_price'] = total_price
        except:
            total_price_error = True
            print('Error fetching total_prices')
        packages.append(response)
    return packages

def filter_single_response(result, departures):
    package = result['package']
    prices = result['prices_by_service_level']
    response = {'package': {}}
    try:
        images = package['images']
        if images[0]:
            response['package']['image'] = images[0]['url']
    except:
        images_error = True
        print('Error fetching image')
    try:
        id = package['id']
        if id:
            response['package']['id'] = id
    except:
        id_error = True
        print('Error fetching id')
    try:
        length = package['length']
        if length:
            response['package']['length'] = length
    except:
        length_error = True
        print('Error fetching length')
    try:
        category = package['category']
        if category:
            response['package']['category'] = category
    except:
        category_error = True
        print('Error fetching category')
    try:
        name = package['name']
        if name:
            response['package']['name'] = name
    except:
        name_error = True
        print('Error fetching name')
    try:
        description = package['description']
        if description:
            response['package']['description'] = description
    except:
        description_error = True
        print('Error fetching description')
    try:
        start_location = package['start_location']
        if start_location:
            response['package']['start_location'] = start_location
        print(start_location)
    except:
        start_location_error = True
        print('Error fetching start location')
    try:
        end_location = package['end_location']
        if end_location:
            response['package']['end_location'] = end_location
    except:
        end_location_error = True
        print('Error fetching end location')
    try:
        total_price = prices[0]['total_price']
        if total_price:
            response['package']['total_price'] = total_price
    except:
        total_price_error = True
        print('Error fetching total_prices')
    try:
        if departures:
            response['package']['departures'] = departures
    except:
        departures_error = True
    try:
        departure_type_name = package['departure_type_name']
        if departure_type_name:
            response['package']['departure_type_name'] = departure_type_name
    except:
        print('Error fetching departure_type_name')
        departures_type_name_error = True
    return response