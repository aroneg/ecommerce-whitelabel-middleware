from queue import Empty
from tracemalloc import start
from unittest import result
from django.shortcuts import render
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import mixins
from rest_framework import generics, authentication, permissions
import os
import requests
from dotenv import load_dotenv

load_dotenv()
KTAPI_BASE_URL = os.getenv('KTAPI_BASE_URL')
API_KEY = os.getenv('API_KEY')
API_SECRET = os.getenv('API_SECRET')
HEADER = os.getenv('HEADER')
TAX_PROFILE_ID = os.getenv('TAX_PROFILE_ID')
CHANNEL_ID = os.getenv('CHANNEL_ID')

# Create your views here.

class CategoriesListView(generics.ListAPIView):
    queryset = ""
    serializer_class = ""
    permission_classes = [AllowAny,]

    def get(self, request):
        url = create_url(request=request)
        req = requests.get(url, headers={"Authorization": HEADER})
        req_json = req.json()
        categories = self.add_category(req_json['results'])
        return Response({'categories': categories}, status=req.status_code)
    
    def add_category(self, results):
        categories = []
        for item in results:
            if item['package']['category'] not in categories and item['package']['category']:
                categories.append(item['package']['category'])
            else:
                continue
        return categories


        

def create_url(request):
    url = f'{KTAPI_BASE_URL}/v1.0/packages/search?tax_profile_id={TAX_PROFILE_ID}&channel_id={CHANNEL_ID}&currency=EUR&occupancy=1=1,0'
    return url